import AsyncStorage from '@react-native-community/async-storage';
import _ from 'lodash';

var Storage = {

  setData: async (key, val) => {
    try {
      val = _.isObject(val) ? JSON.stringify(val) : val;
      await AsyncStorage.setItem(key, val);
      return true;
    } catch (error) {
      return false;
    }
  },

  getData: async (key, defaultValue) => {
    let val;
    try {
      val = await AsyncStorage.getItem(key);
      return JSON.parse(val);
    } catch (error) {
      return defaultValue || {};
    }
  },

  deleteData: async (key) => {
    return await AsyncStorage.removeItem(key);
  },

  clear: async () => {
    return await AsyncStorage.clear();
  }
};

export default Storage;
