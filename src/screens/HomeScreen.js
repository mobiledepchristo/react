import React, { Component } from 'react';
import { StyleSheet, FlatList, Text, View, Alert, TouchableOpacity, TextInput , Modal ,Dimensions , Button } from 'react-native';
import Storage from '../modules/Storage';
import Icon from 'react-native-vector-icons/AntDesign';
import {Card} from 'react-native-elements';
const deviceWidth = Dimensions.get('window').width;
class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.array = []
        this.state = {
          arrayHolder: [],
          textInput_Holder: '',
          editModalVisible:false,
          editTextInput:'',
          inputIdToEdit:''
        };
      }
  
      async componentDidMount () {
        let obj = await Storage.getData("List");
         this.setState({
              arrayHolder: obj 
         });
      }
    
     joinData = async() => {
       this.array =  this.state.arrayHolder || [];
       this.array.push({id:Math.floor(Math.random() * 100), title : this.state.textInput_Holder});
       this.setState({ arrayHolder: [...this.array] });
       this.storeDataToLocal([...this.array]);
     }
    
    FlatListItemSeparator = () => {
      return (
        <View
          style={{
            height: 1,
            width: "100%",
            marginHorizontal:20,
            backgroundColor: "#607D8B",
          }}
        />
      );
    }
    
    GetItem(item){
       Alert.alert(item);
    }
    

    showEditModal = (item) => {
      this.setState({
        editModalVisible:true,
        editTextInput:item.title,
        inputIdToEdit:item.id
      })
    }

    editTodoList = async(id) => {
      console.log('ddd');
       this.array =  this.state.arrayHolder;
       const data  = this.array.map(item => {
       if(item.id === id){
         item.title = this.state.editTextInput;
             return item;
          }
           return item;
        });  
        this.setState({
          arrayHolder:data,
          editModalVisible:false
        });
        this.storeDataToLocal(data);
    }


    deleteTodo = async(id) => {
      this.array =  this.state.arrayHolder;
      this.array = this.array.filter((list) =>list.id !== id);
      this.setState({arrayHolder:this.array});
      this.storeDataToLocal(this.array);
    }
    

    storeDataToLocal = async(data) => {
      await Storage.setData("List", data);
    }
    
      render() {
        return (
          <View style={styles.container}>
            <TextInput
              placeholder="Enter"
              onChangeText={data=>this.setState({textInput_Holder:data})}
              style={styles.textInputStyle}
              //underlineColorAndroid='transparent'
            />
            <TouchableOpacity onPress={this.joinData} activeOpacity={0.7} style={styles.button}>
             <Text style={styles.buttonText}>Add Todo To List</Text>
            </TouchableOpacity>
            <FlatList
             data={this.state.arrayHolder}
             width='100%'
             keyExtractor={(item) => item.id}
             ItemSeparatorComponent={this.FlatListItemSeparator}
             renderItem={({item})=> { 
                 return (
                     <View style={styles.todoDetails}>
                      <Text style={styles.item} onPress={this.GetItem.bind(this,item.title)}>{item.title}</Text>
                      <View style={{flexDirection:'row'}}>
                      <TouchableOpacity onPress={() => this.showEditModal(item)}>
                      <Icon name="edit" size={20} style={styles.iconStyle} />
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => this.deleteTodo(item.id)}>
                      <Icon name="delete" size={20} style={styles.iconStyle} />
                     </TouchableOpacity>
                    </View>
                      </View>
                     );
                    }
                }
            />
            <Modal
            animationType={'fade'}
            transparent={true}
            visible={this.state.editModalVisible}
            onRequestClose={() => {
             this.setState({editModalVisible:false});
           }}>
   
             <View  style={{
               width: '100%',
               height: '100%',
               justifyContent: 'center',
               backgroundColor: 'rgba(100,100,100, 0.5)',
               padding: deviceWidth/20,
             }}>
               <Card  title="Edit" borderRadius={30} >
               <TouchableOpacity
                   hitSlop={{top: 50, bottom: 50, left: 50, right: 50}}
                   onPress={() => {
                     this.setState({editModalVisible:false});
                   }}
                   style={styles.closeButton}>
                     <Icon name="close" size={25} color="#4B9FC5" />
                 </TouchableOpacity>
                 <View style={styles.backgroundStyle} >
                 <TextInput
                   style = {styles.inputStyle}
                   placeholder="Enter a todo"
                   value={this.state.editTextInput}
                   onChangeText = {(newValue) => this.setState({editTextInput:newValue})}
                 />
                 </View>
                  <TouchableOpacity onPress={()=>this.editTodoList(this.state.inputIdToEdit)}>
                  <View style={{flex:1,alignItems:'center', paddingBottom:30}} >
                  <View  >
                    <Text style = {{color: "#4b9fc5", fontSize: 20, fontWeight: 'bold'}}>Save</Text>
                  </View>
                  </View>
                  </TouchableOpacity>
               </Card>
             </View>
           </Modal>
          </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
       container:{
         flex:1,
         justifyContent: 'center',
         alignItems: 'center',
         margin:2, 
       },

       closeButton: {
        position: 'absolute',
        right: 5,
      },
      
       saveButton:{
         alignSelf:"center",
         margin:10,
         paddingBottom:20
       }, 

       backgroundStyle:{
        height:50,
        width:250,
        borderRadius:5,
        borderColor:'black',
        borderWidth:2,
        marginTop:10,
        marginBottom:10
       },

       iconStyle:{
          alignSelf:'center',
          padding:5,
       },

       todoDetails: {
         flexDirection:'row',
         justifyContent:'space-between',
         marginTop:10
       },
    
       item: {
         padding: 10,
         fontSize:18,
         height:44,
       },
    
       textInputStyle: {
        textAlign: 'center',
        height: 40,
        width: '90%',
        borderWidth: 1,
        borderColor: '#4CAF50',
        borderRadius: 7,
        marginTop: 12
      },
     
      button: {     
        width: '90%',
        height: 40,
        padding: 10,
        backgroundColor: '#4CAF50',
        borderRadius: 8,
        marginTop: 10
      },
     
      buttonText: {
        color: '#fff',
        textAlign: 'center',
      },
    })

export default HomeScreen;
