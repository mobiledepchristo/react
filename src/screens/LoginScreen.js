import React,{useState} from 'react';
import { Text, View, StyleSheet ,TextInput, Button } from 'react-native';
import loginApi from '../api/loginApi';

const LoginScreen = ({navigation}) => {
  
 const [user,setUser] = useState('');
 const [pass,setPassword] = useState('');
 const [errorMsg,setErrorMsg] = useState('');

  const login = async() => {
      const response = await loginApi.get();     
      const allowed = response.data.authentication;
      return allowed.some(function (auth) {
        if(auth.username === user.trim() && auth.password == pass.trim()){ 
            navigation.navigate('Home');
            setErrorMsg('');
        }else{
            setErrorMsg('Invalid Username and Password')
        }
    });
  }
     
    return (
      <View style={styles.container}>
        <Text style={styles.loginText} >LoginScreen</Text>
        <View style={styles.backgroundStyle} >
          <TextInput
            style = {styles.inputStyle}
            placeholder="User name"
            value = {user}
            onChangeText={name=>setUser(name)}
          />
         </View>
         <View style={styles.backgroundStyle} > 
          <TextInput
            style = {styles.inputStyle}
            placeholder="Password"
            value = {pass}
            onChangeText={password=>setPassword(password)}
          />
        </View>
         <View style={styles.button}> 
          <Button title="Login"  onPress={() => login()} />
        </View>
        <Text>{errorMsg}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
     container:{
         flex:1,
         alignItems:'center',
         justifyContent:'center'
     },
     backgroundStyle:{
        backgroundColor:'#F0EEEE', 
        height:50,
        width:250,
        borderRadius:5,
        marginHorizontal:15,
        borderColor:'black',
        marginTop:10,
        marginBottom:10
     },
     loginText:{
         fontSize:20,
         fontWeight:'bold'
     },
     inputStyle:{
         flex:1,
     },
     button:{
       width:180        
     }       
})

export default LoginScreen;
