import * as React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer } from '@react-navigation/native';
import { createStackNavigator } from 'react-navigation-stack';
import LoginScreen from './src/screens/LoginScreen';
import HomeScreen from './src/screens/HomeScreen';


const navigator = createStackNavigator({
  Login:LoginScreen,
  Home:HomeScreen
},
{
  initialRouteName:'Login',
  defaultNavigationOptions:{
    title:'Todo App'
  }
},
);
const App = createAppContainer(navigator);

export default () => {
  return <App/>
};